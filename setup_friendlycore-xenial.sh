#!/bin/sh
set -e

sudo apt update
sudo apt upgrade
apt install locate tree dtrx mosh
#f ro baresip:
sudo apt install pkg-config libswscale-dev libasound2-dev libopus-dev libpulse-dev libopencore-amrnb-dev pulseaudio-module-zeroconf
 
# make gpios available to gpio group:
cp gpio_udev /etc/udev/rules.d/99-com.rules
# but to be effective we first need to create the gpio group and add it to some user
groupadd gpio 
usermod -a -G gpio pi

# install libre for baresip
git clone https://github.com/creytiv/re.git
cd re
make RELEASE=1
sudo make RELEASE=1 install
sudo ldconfig
cd

#instal librem for baresip
git clone https://github.com/creytiv/re..git
cd rem
make
sudo make install
sudo ldconfig
cd 

# install baresip 

git clone https://github.com/yangaphero/baresip-rpi.git
cd baresip-rpi
make RELEASE=1
sudo make RELEASE=1 install
cd
# change the .baresip/config file to 
ringback_disabled yes
audio_source  	pulse
audio_player 	pulse
audio alert 	pulse

#insert things into the pulseaudio default connf
cat pulse_aec >> /etc/pulse/default.conf
# to be run as the user
systemctl --user enable pulseaudio

git clone https://gitlab.com/mathemathilda/orangePiZeroMFRC522.git



# install ncat through nmap:
apt install nmap netcat
apt install inotify-tools

# little test setup:
# the -q option seems to not work with the server side -k option, so we need to set up inotifywait and transform file change into program execution.

# on the server run 
# @boot :
echo "11" > /sys/class/gpio/export
echo "12" > /sys/class/gpio/export
echo "in"  > /sys/class/gpio/gpio12/direction
echo "out"  > /sys/class/gpio/gpio11/direction
echo "0"    > /sys/class/gpio/gpio11/value
inotifywait -q -m -e close_write doorbell | while read -r filename event; do echo "1"    > /sys/class/gpio/gpio11/value;   aplay -D plughw:CARD=Codec,DEV=0 test.wav;  echo "0"    > /sys/class/gpio/gpio11/value ; done
inotifywait -q -m -e close_write /sys/class/gpio/gpio12/value | while read -r filename event; do  baresip -e "d sip:test@10.6.26.167"   ; done
# and 
ncat -k -v -l -p 5555  >   test

# on the local machine run:

 echo "0" |  nc -c 10.6.26.61 5555
# adjust the IP adress
 


# add the /dev/spidev1.0 as a spi port:
# enable a source in /etc/apt/sources.list

apt source gcc-defaults

# download precompiled gcc toolchain
wget https://developer.arm.com/-/media/Files/downloads/gnu-rm/9-2019q4/gcc-arm-none-eabi-9-2019-q4-major-aarch64-linux.tar.bz2?revision=4583ce78-e7e7-459a-ad9f-bff8e94839f1&la=en&hash=550DB9C0184B7C70B6C020A5DCBB9D1E156264B7
# install as given in the readme




git clone https://github.com/friendlyarm/kernel-rockchip --depth 1 -b nanopi4-linux-v4.4.y kernel-rockchip
cd kernel-rockchip
vim arch/arm64/boot/dts/rockchip/rk3399-nanopi4-common.dtsi
# enabl spi1 and disable uart4
make ARCH=arm64 nanopi4_linux_defconfig
export PATH=/opt/FriendlyARM/toolchain/6.4-aarch64/bin/:$PATH
make ARCH=arm64 nanopi4-images 


#   rust   #
# 
apt install software-properties-common # thats for add-apt-repository
add-apt-repository ppa:linaro-maintainers/toolchain
apt update
apt install gcc-**version**-aarch64-linux-gnud


# followinng for debugging:
#opkg install librt make
#git clone https://github.com/larsks/gpio-watch.git

#cd gpio-watch
#export CC=gcc && make
#cd ..
#cp relaislogger /etc/gpio-scripts/203

