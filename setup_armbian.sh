#!/bin/sh
set -e

###sudo apt update
###sudo apt install python3-pip python3-dev -y
if [ ! -d orangePiZeroMFRC522 ] 
then 
	git clone https://gitlab.com/mathemathilda/orangePiZeroMFRC522.git
else 
	echo "orangepiZeroMFRC522 repository already cloned"
fi

# gcc to be able to compile SPI-py
# seems as if the hardware spi is not really acccessible, but bitbanging is better than nothing

# to find out which pin is which go to http://linux-sunxi.org/GPIO, there is a description...
# use spi-gpi-custom to make a new spi port, parameters adapted from https://randomcoderdude.wordpress.com/2013/08/15/spi-over-gpio-in-openwrt/
# so the resulting cryptic string is:

# insmod spi-gpio-custom bus0=1,14,15,16,0,1000,13
# the insmod command needs to be called once for the module later  to work.
# so for persistence manifest it as a module
#cp  /root/orangePiZeroMFRC522/openwrt/spi-gpio-custom /etc/modules.d

# update setuptools, the default installation seems to have somme permissions wrong, see here:
# https://stackoverflow.com/questions/35991403/pip-install-unroll-python-setup-py-egg-info-failed-with-error-code-1

pip install setuptools==58.2.0
install_python_dependency (){
	folder=$(basename $1 | sed 's/.git//')
	if [ -d $folder ]
	then 
		echo $1" already obtained"
	else 
		git clone $1
		cd $folder
		sudo python3 setup.py install
		cd ..
	fi
}

pip install spidev
cd ~/
#  python3 SPI for the MFRC522 reader
install_python_dependency https://github.com/lthiery/SPI-Py.git
# python gpio library for the relay
install_python_dependency https://github.com/karabek/nanopi_duo_gpio_pyH3

# install initdoor to launch door.py at bootup
cp /root/orangePiZeroMFRC522/openwrt/doorinitscript /etc/init.d/door
# not sure whether the following is really necessary:
reload_config
# 
touch /root/dooradmin
touch /root/dooraccess

# followinng for debugging:
#opkg install librt make
#git clone https://github.com/larsks/gpio-watch.git

#cd gpio-watch
#export CC=gcc && make
#cd ..
#cp relaislogger /etc/gpio-scripts/203

