#!/bin/sh
set -e

#opkg update
# following should be preinstalled on the image
#opkg install python python-dev python-setuptools kmod-spi-gpio kmod-spi-dev kmod-spi-gpio-custom git-http ca-bundle 
# these ones are new:
#opkg install gcc make python-pip
opkg update
opkg install openssh-sftp-server
 
git clone https://gitlab.com/mathemathilda/orangePiZeroMFRC522.git

# gcc to be able to compile SPI-py
# seems as if the hardware spi is not really acccessible, but bitbanging is better than nothing

# to find out which pin is which go to http://linux-sunxi.org/GPIO, there is a description...
# use spi-gpi-custom to make a new spi port, parameters adapted from https://randomcoderdude.wordpress.com/2013/08/15/spi-over-gpio-in-openwrt/
# so the resulting cryptic string is:

insmod spi-gpio-custom bus0=1,14,15,16,0,1000,13
# the insmod command needs to be called once for the module later  to work.
# so for persistence manifest it as a module
cp  /root/orangePiZeroMFRC522/openwrt/spi-gpio-custom /etc/modules.d

# update setuptools, the default installation seems to have somme permissions wrong, see here:
# https://stackoverflow.com/questions/35991403/pip-install-unroll-python-setup-py-egg-info-failed-with-error-code-1

pip install --upgrade setuptools

pip install spidev
cd ~/
#  python SPI for the MFRC522 reader
git clone https://github.com/lthiery/SPI-Py.git
cd SPI-Py
python setup.py install
cd ..

# python gpio library for the relay
git clone https://github.com/karabek/nanopi_duo_gpio_pyH3
cd nanopi_duo_gpio_pyH3
python setup.py install
cd ..
# install initdoor to launch door.py at bootup
cp /root/orangePiZeroMFRC522/openwrt/doorinitscript /etc/init.d/door
# not sure whether the following is really necessary:
reload_config
# 
touch /root/dooradmin
touch /root/dooraccess

# followinng for debugging:
#opkg install librt make
#git clone https://github.com/larsks/gpio-watch.git

#cd gpio-watch
#export CC=gcc && make
#cd ..
#cp relaislogger /etc/gpio-scripts/203

