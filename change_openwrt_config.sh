#!/bin/bash

cat  ~/.ssh/id_rsa.pub > auths
cat sshkey.pub >> auths
scp auths root@\[$1\]:/etc/dropbear/authorized_keys
scp network root@\[$1\]:/etc/config/network
scp dhcp root@\[$1\]:/etc/config/dhcp
read -e -p " enter hostname:   " hostname
cat system | sed -e "s/foodcoopdoor2/$hostname/" > newsystem
scp newsystem root@\[$1\]:/etc/config/system 
scp dropbear  root@\[$1\]:/etc/config/dropbear
scp setup.sh  root@\[$1\]:/root/setup.sh
#reboot is important otherwise the inforation is prone to be lost on poweroff
scp -i sshkey root@frontdoor.lan:doora* .
scp  dooraccess dooradmin root@\[$1\]:/root/

ssh root@\[$1\] 'reboot'
