# OpenWrt setup scripts for NanoPi Duo as RFID Access Control System 

This is the part that is supposed to be cloned to your local machine for easy setup of a NanoPi as Acces Control System.
**change_openwrt_config.sh** is supposed to copy  appropriate config files to the nanopi duo via scp.  
**setup.sh** is to be executet on the Pi. it is copied with  the other config files to the nanopi.  
For conveniennce there is an image with /dev/mem enabled and all needed packages preinstalled.
As of 2018-12-22 it seems to be only possible to compile openwrt images on headless machines, so you might as well just stick with this image.  

## Workflow:  

expand the image to ~500MB:  
otherwise you wont be able to install stuff or update.
`dd if=/dev/zero bs=4M count=128 of=extended.img`  
`dd if=openwrt-sunxi-cortexa7-sun8i-h3-nanopi-neo-squashfs-sdcard.img of=extended.img conv=notrunc`  
put the SD card in your computer and find out its name with `lsblk`  
flash image on sd card:  
`sudo dd if=extended.img bs=4M of= /dev/sdX`  
no guarantee that the image name stays the same...  
for safety do a `sync`, put the SD card in the NanoPi, power it up and connect via Link-Local.  
find its ip:  
`ping -6 -I INTERFCE ff02::01`  
where INTERFACE is something like enp0s20u2 or eth0 or whatever your ethernet port is.
There should be one or two entries, depending on whether you own machine is listed.
try a ssh:  
`ssh root@XXXXXXXXXXXXXXXXXXXXX`  
Works? Fine! copy the configfiles using
` ./change_openwrt_config.sh XXXXXXXXXXXXXXXXXXXXX`  
You will be asked to enter a hostname.  
ssh back to the NanoPi and reboot it. If you just cut the power, it might forget the new config you gave it, guess it may keep it in RAM, so maybe a sync also does the job.  

now the NanoPi should refrain from doing DHCP or other nasty things, and it should have your public ssh-key on it, asssuming that its calle id_rsa and in your .ssh folder  
**note** as of of ssh-copy-id has problems with openwrt, so for more keys I only see the option of adding them manually.  

you can now unplug the nanopi and connect it to your network  

Login to your Pi and there should be a setup.sh.  
Execute that and you should be ready to go with a hopefully working "Access Control Pi".  
finally enable the door service, so the pi begins reading at boot:  
` service door enable`  

# Armbian setup scripts (WIP):

download the latest image from https://www.armbian.com/nanopi-duo/ 
extract the image 
`7z x XXXXXXXXXX.7z` 

find the sd card using 
`lsblk`
copy the image file to an sd card  supposing that `lsblk` has shown the sd card to be /dev/sdb (check that):
`sudo dd if=XXXXXXXXX.img of=/dev/sdb status=progress bs=4M`
insert the sd card into the nanopi, power it on and wait until it starts to bling in a heartbeat mode.
search for the locallink adress using 
`ping -6 -I INTERFCE ff02::01`
where INTERFACE is something like enp0s20u2 or eth0 or whatever your ethernet port is. 
There should be one or two entries, depending on whether you own machine is listed.
try a ssh to the the given adress keeping the interface in the adress:  
`ssh root@XXXXXXXXXXXXXXXXXXXXX%INTERFACE:`
default armbian password is 1234
when asked, create a new user USERNAME 
copy  your public key to the nanopi:
`ssh-copy-id `

to disable password login edit `/etc/ssh/sshd_config` as follows 
Find ChallengeResponseAuthentication and set to no:

ChallengeResponseAuthentication no

Next, find PasswordAuthentication set to no too:

PasswordAuthentication no

Search for UsePAM and set to no, too:

UsePAM no

reload ssh config:
`/etc/init.d/ssh reload`

set hostname in 

enable sound with:

` apt-get update`
` apt-get install libasound2 alsa-base alsa-utils`

check soundcard with:

`aplay -l `

install bernard, a headless mumble client:

apt-get install libopenal-dev libopus-dev libasound2-dev git ffmpeg screen pkg-config
apt install omxplayer  # does not seem to exist as this any more

wget https://dl.google.com/go/go1.14.2.linux-armv6l.tar.gz
tar -C /usr/local -xzf go1.14.2.linux-armv6l.tar.gz

nano ~/.profile
PATH=$PATH:/usr/local/go/bin

GOPATH=$HOME/talkkonnect/gocode
